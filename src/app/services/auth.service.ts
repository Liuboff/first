import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  register() {}

  login(user: User) {
    return this.http.post('login', user);
  }

  logout(){
    localStorage.setItem('isLoggedIn', 'false');
    this.router.navigate(['login']);
  }

  isLoggedIn():boolean {
    return localStorage.getItem('isLoggedIn') === 'true' ? true : false;
  }
}
