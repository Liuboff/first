import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addtext'
})
export class AddtextPipe implements PipeTransform {

  transform(value: string, str?: string): string {

    return str === undefined ? value : `${str} ${value}`;
  }

}
