import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder, 
    private http: HttpClient, 
    private router: Router) { }

  public signupForm!: FormGroup;

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]{6,}')]],
      confirmPassword: ['', Validators.required]
    }, {validator: this.checkPasswords});
  }

  checkPasswords(signupForm: FormGroup) {
    const pass = signupForm.controls['password'].value;
    const confirmPass = signupForm.controls['confirmPassword'].value;

    return pass === confirmPass ? null : { notSame: true };
  }

  signUp() {
    this.http.post<any>('http://localhost:5000/registered', this.signupForm.value)
      .subscribe({
        next: (response) => {
          alert('Sigup successful');
          this.signupForm.reset();
          this.router.navigate(['login']);
        },
        error: err => {
          alert('Somthing went wrong')
        }
      })
  }
}