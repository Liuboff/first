import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

import { HomeService } from '../../services/home.service';
import { Post } from '..//../models/post';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private homeService: HomeService, 
    private router: ActivatedRoute) { }
    
  username: string = 'user';
  posts: Post[] = [];


  ngOnInit(): void {
    this.getPosts();
    // this.username = this.router.snapshot.params['login'];
  }

  getPosts(): void {
    this.homeService.getPosts().subscribe(posts => this.posts = posts);
  }

  panelOpenState = false;

}
