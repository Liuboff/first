import { Component, OnInit } from '@angular/core';
import { Router, Routes } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpClient } from '@angular/common/http';

import { User } from 'src/app/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm!: FormGroup;

  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    private http: HttpClient) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }

  login() {
    this.http.get<User[]>('http://localhost:5000/registered')
      .subscribe({
        next: res => {
          const user = res.find((user: any) => {
            return user.email === this.loginForm.value.email && user.password === this.loginForm.value.password
          });
          if (user) {
            alert("Login success");
            localStorage.setItem("isLoggedIn", "true");
            this.loginForm.reset();
            this.router.navigate([''])
          } else {
            alert("User not found");
          }
        },
        error: err => {
          alert("Something went wrong");
        }
      })
  }

}
